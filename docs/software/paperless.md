# Paperless

[Paperless](https://github.com/danielquinn/paperless) is a document management server. Point it at a folder on your NAS that your scanner is set to scan to, scan all your paper docs, have a searchable secure document archive.

## Access

It is available at [http://paperless.{{ domain }}/](http://paperless.{{ domain }}/)