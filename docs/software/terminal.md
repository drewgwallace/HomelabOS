# Terminal

[Butterfly](georgeyord/butterfly-web-terminal) is available as a simple web terminal.

## Access

It is available at [http://terminal.{{ domain }}/](http://terminal.{{ domain }}/)