# Sonerezh

[Sonerezh](https://www.sonerezh.bzh/) is a music streaming server. Point it at the music collection on your NAS and you're off.

## Access

It is available at [http://music.{{ domain }}/](http://music.{{ domain }}/)